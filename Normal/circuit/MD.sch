EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MD-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "MotorDriver with Encoder Calculator"
Date "2016-02-07"
Rev ""
Comp "Author is Yukiya"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Q_NMOS_SDG HighFetLeft1
U 1 1 56B5C0DA
P 4850 4650
F 0 "HighFetLeft1" H 5150 4700 50  0000 R CNN
F 1 "Q_NMOS_SDG" H 5500 4600 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Vertical_LargePads" H 5050 4750 50  0001 C CNN
F 3 "" H 4850 4650 50  0000 C CNN
	1    4850 4650
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_SDG LowFetLeft1
U 1 1 56B61561
P 4850 5800
F 0 "LowFetLeft1" H 5150 5850 50  0000 R CNN
F 1 "Q_NMOS_SDG" H 5500 5750 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Vertical" H 5050 5900 50  0001 C CNN
F 3 "" H 4850 5800 50  0000 C CNN
	1    4850 5800
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_SDG LowFetRight1
U 1 1 56B61696
P 7100 5800
F 0 "LowFetRight1" H 7400 5850 50  0000 R CNN
F 1 "Q_NMOS_SDG" H 7750 5750 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Vertical" H 7300 5900 50  0001 C CNN
F 3 "" H 7100 5800 50  0000 C CNN
	1    7100 5800
	-1   0    0    -1  
$EndComp
$Comp
L PHOTORESISTOR U1
U 1 1 56B61B5C
P 4250 1200
F 0 "U1" H 4260 1520 50  0000 C CNN
F 1 "PHOTORESISTOR" H 4260 880 50  0000 C CNN
F 2 "Housings_DIP:DIP-4_W7.62mm" H 4250 1200 50  0001 C CNN
F 3 "" H 4250 1200 50  0000 C CNN
	1    4250 1200
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 56B6F42B
P 3400 1000
F 0 "R8" V 3480 1000 50  0000 C CNN
F 1 "R" V 3400 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3330 1000 50  0001 C CNN
F 3 "" H 3400 1000 50  0000 C CNN
	1    3400 1000
	0    1    1    0   
$EndComp
Text Label 3350 1400 0    60   ~ 0
GND
Text Label 5050 1400 0    60   ~ 0
Drive_Vcc_High_0
Text Label 5050 1000 0    60   ~ 0
Drive_High_0
Text Label 2900 1000 0    60   ~ 0
High_0
$Comp
L PHOTORESISTOR U2
U 1 1 56B70BFF
P 4250 2050
F 0 "U2" H 4260 2370 50  0000 C CNN
F 1 "PHOTORESISTOR" H 4260 1730 50  0000 C CNN
F 2 "Housings_DIP:DIP-4_W7.62mm" H 4250 2050 50  0001 C CNN
F 3 "" H 4250 2050 50  0000 C CNN
	1    4250 2050
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 56B70C05
P 3400 1850
F 0 "R9" V 3480 1850 50  0000 C CNN
F 1 "R" V 3400 1850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3330 1850 50  0001 C CNN
F 3 "" H 3400 1850 50  0000 C CNN
	1    3400 1850
	0    1    1    0   
$EndComp
Text Label 3350 2250 0    60   ~ 0
GND
Text Label 5050 2250 0    60   ~ 0
Drive_Vcc_High_1
Text Label 5050 1850 0    60   ~ 0
Drive_High_1
Text Label 2900 1850 0    60   ~ 0
High_1
$Comp
L PHOTORESISTOR U3
U 1 1 56B70FF0
P 7400 1200
F 0 "U3" H 7410 1520 50  0000 C CNN
F 1 "PHOTORESISTOR" H 7410 880 50  0000 C CNN
F 2 "Housings_DIP:DIP-4_W7.62mm" H 7400 1200 50  0001 C CNN
F 3 "" H 7400 1200 50  0000 C CNN
	1    7400 1200
	1    0    0    -1  
$EndComp
$Comp
L R R18
U 1 1 56B70FF6
P 6550 1000
F 0 "R18" V 6630 1000 50  0000 C CNN
F 1 "R" V 6550 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6480 1000 50  0001 C CNN
F 3 "" H 6550 1000 50  0000 C CNN
	1    6550 1000
	0    1    1    0   
$EndComp
Text Label 6500 1400 0    60   ~ 0
GND
Text Label 8200 1400 0    60   ~ 0
Drive_Vcc
Text Label 8200 1000 0    60   ~ 0
Drive_Low_0
Text Label 6050 1000 0    60   ~ 0
Low_0
$Comp
L PHOTORESISTOR U4
U 1 1 56B7100F
P 7400 2050
F 0 "U4" H 7410 2370 50  0000 C CNN
F 1 "PHOTORESISTOR" H 7410 1730 50  0000 C CNN
F 2 "Housings_DIP:DIP-4_W7.62mm" H 7400 2050 50  0001 C CNN
F 3 "" H 7400 2050 50  0000 C CNN
	1    7400 2050
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 56B71015
P 6550 1850
F 0 "R19" V 6630 1850 50  0000 C CNN
F 1 "R" V 6550 1850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 6480 1850 50  0001 C CNN
F 3 "" H 6550 1850 50  0000 C CNN
	1    6550 1850
	0    1    1    0   
$EndComp
Text Label 6500 2250 0    60   ~ 0
GND
Text Label 8200 2250 0    60   ~ 0
Drive_Vcc
Text Label 8200 1850 0    60   ~ 0
Drive_Low_1
Text Label 6050 1850 0    60   ~ 0
Low_1
Text Label 6550 2900 0    60   ~ 0
Motor_+
Text Label 6550 3500 0    60   ~ 0
Motor_-
Text Label 5450 5250 0    60   ~ 0
Motor_+
Text Label 6250 5250 0    60   ~ 0
Motor_-
Text Label 5950 4150 0    60   ~ 0
Drive_Vcc
Text Label 5950 6250 0    60   ~ 0
Drive_GND
Text Label 2700 4650 0    60   ~ 0
Drive_High_0
Text Label 8500 4650 0    60   ~ 0
Drive_High_1
Text Label 3100 5800 0    60   ~ 0
Drive_Low_0
Text Label 8450 5800 0    60   ~ 0
Drive_Low_1
$Comp
L R R15
U 1 1 56B73A6A
P 4400 5800
F 0 "R15" V 4480 5800 50  0000 C CNN
F 1 "R" V 4400 5800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 4330 5800 50  0001 C CNN
F 3 "" H 4400 5800 50  0000 C CNN
	1    4400 5800
	0    1    1    0   
$EndComp
$Comp
L R R20
U 1 1 56B73B08
P 7400 4950
F 0 "R20" V 7480 4950 50  0000 C CNN
F 1 "R" V 7400 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7330 4950 50  0001 C CNN
F 3 "" H 7400 4950 50  0000 C CNN
	1    7400 4950
	-1   0    0    1   
$EndComp
$Comp
L R R21
U 1 1 56B73C75
P 7550 5800
F 0 "R21" V 7630 5800 50  0000 C CNN
F 1 "R" V 7550 5800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7480 5800 50  0001 C CNN
F 3 "" H 7550 5800 50  0000 C CNN
	1    7550 5800
	0    1    1    0   
$EndComp
$Comp
L C BootStrap1
U 1 1 56B74093
P 4050 5000
F 0 "BootStrap1" H 4075 5100 50  0000 L CNN
F 1 "C" H 4075 4900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L4_W2.5_P2.5" H 4088 4850 50  0001 C CNN
F 3 "" H 4050 5000 50  0000 C CNN
	1    4050 5000
	1    0    0    -1  
$EndComp
$Comp
L C BootStrap2
U 1 1 56B741B1
P 7800 5000
F 0 "BootStrap2" H 7825 5100 50  0000 L CNN
F 1 "C" H 7825 4900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L4_W2.5_P2.5" H 7838 4850 50  0001 C CNN
F 3 "" H 7800 5000 50  0000 C CNN
	1    7800 5000
	1    0    0    -1  
$EndComp
$Comp
L D D2
U 1 1 56B74596
P 4350 4250
F 0 "D2" H 4350 4350 50  0000 C CNN
F 1 "D" H 4350 4150 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 4350 4250 50  0001 C CNN
F 3 "" H 4350 4250 50  0000 C CNN
	1    4350 4250
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky Snub1
U 1 1 56B7531A
P 5450 4900
F 0 "Snub1" H 5450 5000 50  0000 C CNN
F 1 "D_Schottky" H 5450 4800 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQPE1" H 5450 4900 50  0001 C CNN
F 3 "" H 5450 4900 50  0000 C CNN
	1    5450 4900
	0    1    1    0   
$EndComp
$Comp
L D_Schottky Snub3
U 1 1 56B75407
P 6450 4900
F 0 "Snub3" H 6450 5000 50  0000 C CNN
F 1 "D_Schottky" H 6450 4800 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQPE1" H 6450 4900 50  0001 C CNN
F 3 "" H 6450 4900 50  0000 C CNN
	1    6450 4900
	0    1    1    0   
$EndComp
$Comp
L D_Schottky Snub2
U 1 1 56B75559
P 5450 5650
F 0 "Snub2" H 5450 5750 50  0000 C CNN
F 1 "D_Schottky" H 5450 5550 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQPE1" H 5450 5650 50  0001 C CNN
F 3 "" H 5450 5650 50  0000 C CNN
	1    5450 5650
	0    1    1    0   
$EndComp
$Comp
L D_Schottky Snub4
U 1 1 56B7555F
P 6450 5650
F 0 "Snub4" H 6450 5750 50  0000 C CNN
F 1 "D_Schottky" H 6450 5550 50  0000 C CNN
F 2 "Buttons_Switches_SMD:SW_SPST_EVQPE1" H 6450 5650 50  0001 C CNN
F 3 "" H 6450 5650 50  0000 C CNN
	1    6450 5650
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 P1
U 1 1 56C7FDFE
P 7100 2900
F 0 "P1" H 7100 3000 50  0000 C CNN
F 1 "CONN_01X01" V 7200 2900 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01" H 7100 2900 50  0001 C CNN
F 3 "" H 7100 2900 50  0000 C CNN
	1    7100 2900
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P2
U 1 1 56C7FEED
P 7100 3500
F 0 "P2" H 7100 3600 50  0000 C CNN
F 1 "CONN_01X01" V 7200 3500 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01" H 7100 3500 50  0001 C CNN
F 3 "" H 7100 3500 50  0000 C CNN
	1    7100 3500
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X19 P3
U 1 1 56D584CD
P 2050 3550
F 0 "P3" H 2050 4550 50  0000 C CNN
F 1 "CONN_02X19" V 2050 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x19" H 2050 2550 50  0001 C CNN
F 3 "" H 2050 2550 50  0000 C CNN
	1    2050 3550
	1    0    0    -1  
$EndComp
Text Label 1350 2650 0    60   ~ 0
High_0
Text Label 1350 2750 0    60   ~ 0
High_1
Text Label 1350 2850 0    60   ~ 0
Low_0
Text Label 1350 2950 0    60   ~ 0
Low_1
Text Label 1350 3050 0    60   ~ 0
GND
Text Label 2550 3150 0    60   ~ 0
Drive_VCC
Text Label 2550 3850 0    60   ~ 0
Drive_GND
Wire Wire Line
	2900 1000 3250 1000
Wire Wire Line
	3550 1000 3650 1000
Wire Wire Line
	3650 1400 3350 1400
Wire Wire Line
	5050 1400 4850 1400
Wire Wire Line
	4850 1000 5050 1000
Wire Wire Line
	2900 1850 3250 1850
Wire Wire Line
	3550 1850 3650 1850
Wire Wire Line
	3650 2250 3350 2250
Wire Wire Line
	5050 2250 4850 2250
Wire Wire Line
	4850 1850 5050 1850
Wire Wire Line
	6050 1000 6400 1000
Wire Wire Line
	6700 1000 6800 1000
Wire Wire Line
	6800 1400 6500 1400
Wire Wire Line
	8200 1400 8000 1400
Wire Wire Line
	8000 1000 8200 1000
Wire Wire Line
	6050 1850 6400 1850
Wire Wire Line
	6700 1850 6800 1850
Wire Wire Line
	6800 2250 6500 2250
Wire Wire Line
	8200 2250 8000 2250
Wire Wire Line
	8000 1850 8200 1850
Wire Wire Line
	4950 4850 4950 5600
Wire Wire Line
	4050 5250 5450 5250
Connection ~ 4950 5250
Wire Wire Line
	6250 5250 7800 5250
Wire Wire Line
	7000 4850 7000 5600
Connection ~ 7000 5250
Wire Wire Line
	5950 4450 5950 4150
Wire Wire Line
	4950 4450 7000 4450
Connection ~ 5950 4450
Wire Wire Line
	4950 6000 7000 6000
Wire Wire Line
	5950 6250 5950 6000
Connection ~ 5950 6000
Wire Wire Line
	7300 5800 7400 5800
Wire Wire Line
	4550 5800 4650 5800
Wire Wire Line
	7800 5250 7800 5150
Wire Wire Line
	4050 5250 4050 5150
Wire Wire Line
	7700 5800 8450 5800
Wire Wire Line
	3100 5800 4250 5800
Wire Wire Line
	5450 4750 5450 4450
Connection ~ 5450 4450
Wire Wire Line
	5450 5050 5450 5500
Connection ~ 5450 5250
Wire Wire Line
	5450 5800 5450 6000
Connection ~ 5450 6000
Wire Wire Line
	6450 5800 6450 6000
Connection ~ 6450 6000
Wire Wire Line
	6450 5050 6450 5500
Connection ~ 6450 5250
Wire Wire Line
	6450 4750 6450 4450
Connection ~ 6450 4450
Wire Wire Line
	6550 3500 6900 3500
Wire Wire Line
	6550 2900 6900 2900
Wire Wire Line
	1350 2650 2300 2650
Wire Wire Line
	1350 2750 2300 2750
Connection ~ 1800 2750
Wire Wire Line
	1350 2850 2300 2850
Connection ~ 1800 2850
Wire Wire Line
	1350 2950 2300 2950
Connection ~ 1800 2950
Wire Wire Line
	1350 3050 2300 3050
Connection ~ 1800 3050
Connection ~ 1800 2650
Wire Wire Line
	1800 3150 1800 3750
Connection ~ 1800 3250
Connection ~ 1800 3350
Connection ~ 1800 3450
Connection ~ 1800 3550
Connection ~ 1800 3650
Wire Wire Line
	2300 3150 2300 3750
Connection ~ 2300 3250
Connection ~ 2300 3350
Connection ~ 2300 3450
Connection ~ 2300 3550
Connection ~ 2300 3650
Wire Wire Line
	1800 3150 2550 3150
Wire Wire Line
	1800 3850 1800 4450
Connection ~ 1800 3950
Wire Wire Line
	2300 3850 2300 4450
Connection ~ 2300 3950
Connection ~ 2300 4050
Connection ~ 1800 4050
Connection ~ 1800 4150
Connection ~ 2300 4150
Connection ~ 2300 4250
Connection ~ 1800 4250
Connection ~ 1800 4350
Connection ~ 2300 4350
Wire Wire Line
	1800 3850 2550 3850
Connection ~ 2300 3150
Connection ~ 2300 3850
$Comp
L R R2
U 1 1 56E24FA0
P 7550 5600
F 0 "R2" V 7630 5600 50  0000 C CNN
F 1 "R" V 7550 5600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7480 5600 50  0001 C CNN
F 3 "" H 7550 5600 50  0000 C CNN
	1    7550 5600
	0    1    -1   0   
$EndComp
Text Label 7800 5600 0    60   ~ 0
Drive_GND
Wire Wire Line
	7700 5600 7800 5600
Wire Wire Line
	7400 5600 7350 5600
Wire Wire Line
	7350 5600 7350 5800
Connection ~ 7350 5800
$Comp
L R R1
U 1 1 56E2559A
P 4600 4950
F 0 "R1" V 4680 4950 50  0000 C CNN
F 1 "R" V 4600 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 4530 4950 50  0001 C CNN
F 3 "" H 4600 4950 50  0000 C CNN
	1    4600 4950
	1    0    0    1   
$EndComp
$Comp
L R R3
U 1 1 56E25955
P 4400 5650
F 0 "R3" V 4480 5650 50  0000 C CNN
F 1 "R" V 4400 5650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 4330 5650 50  0001 C CNN
F 3 "" H 4400 5650 50  0000 C CNN
	1    4400 5650
	0    1    -1   0   
$EndComp
Text Label 4150 5650 2    60   ~ 0
Drive_GND
Wire Wire Line
	4250 5650 4150 5650
Connection ~ 4600 4650
Wire Wire Line
	4550 5650 4600 5650
Wire Wire Line
	4600 5650 4600 5800
Connection ~ 4600 5800
Wire Wire Line
	4500 4250 7200 4250
Connection ~ 5950 4250
Wire Wire Line
	4200 4250 4050 4250
Wire Wire Line
	4050 4250 4050 4850
Connection ~ 4050 4500
Wire Wire Line
	4600 4800 4600 4650
Wire Wire Line
	4600 5100 4600 5250
Connection ~ 4600 5250
Wire Wire Line
	2700 4650 4650 4650
Wire Wire Line
	2700 4500 4050 4500
Text Label 2700 4500 0    60   ~ 0
Drive_Vcc_High_0
$Comp
L Q_NMOS_SDG HighFetRight1
U 1 1 56B615BC
P 7100 4650
F 0 "HighFetRight1" H 7400 4700 50  0000 R CNN
F 1 "Q_NMOS_SDG" H 7750 4600 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Vertical" H 7300 4750 50  0001 C CNN
F 3 "" H 7100 4650 50  0000 C CNN
	1    7100 4650
	-1   0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 56E4ECE2
P 7350 4250
F 0 "D1" H 7350 4350 50  0000 C CNN
F 1 "D" H 7350 4150 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-35_SOD27_Horizontal_RM10" H 7350 4250 50  0001 C CNN
F 3 "" H 7350 4250 50  0000 C CNN
	1    7350 4250
	-1   0    0    1   
$EndComp
Text Label 8500 4450 0    60   ~ 0
Drive_Vcc_High_1
Wire Wire Line
	8500 4650 7300 4650
Wire Wire Line
	7500 4250 7800 4250
Wire Wire Line
	7800 4250 7800 4850
Wire Wire Line
	8500 4450 7800 4450
Connection ~ 7800 4450
Wire Wire Line
	7400 4800 7400 4650
Connection ~ 7400 4650
Wire Wire Line
	7400 5100 7400 5250
Connection ~ 7400 5250
$EndSCHEMATC
