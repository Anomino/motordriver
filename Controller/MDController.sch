EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MDController-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X19 P5
U 1 1 56D85999
P 7100 2100
F 0 "P5" H 7100 3100 50  0000 C CNN
F 1 "CONN_02X19" V 7100 2100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19" H 7100 1100 50  0001 C CNN
F 3 "" H 7100 1100 50  0000 C CNN
	1    7100 2100
	1    0    0    -1  
$EndComp
Text Label 6300 1200 0    60   ~ 0
High_0_0
Text Label 6300 1300 0    60   ~ 0
High_0_1
Text Label 6300 1400 0    60   ~ 0
Low_0_0
Text Label 6300 1500 0    60   ~ 0
Low_0_1
Text Label 6300 1600 0    60   ~ 0
GND
Wire Wire Line
	6300 1200 7350 1200
Wire Wire Line
	6300 1300 7350 1300
Wire Wire Line
	6300 1400 7350 1400
Wire Wire Line
	6300 1500 7350 1500
Wire Wire Line
	6300 1600 7350 1600
Connection ~ 6850 1200
Connection ~ 6850 1300
Connection ~ 6850 1400
Connection ~ 6850 1500
Connection ~ 6850 1600
Wire Wire Line
	6850 1700 6850 2300
Connection ~ 6850 1800
Connection ~ 6850 1900
Connection ~ 6850 2000
Connection ~ 6850 2100
Wire Wire Line
	7350 1700 7350 2300
Connection ~ 7350 2100
Connection ~ 7350 2000
Connection ~ 7350 1900
Connection ~ 7350 1800
Wire Wire Line
	6300 1700 7350 1700
Connection ~ 6850 2200
Connection ~ 7350 2200
Wire Wire Line
	6850 2400 6850 3000
Connection ~ 6850 2500
Connection ~ 6850 2600
Connection ~ 6850 2700
Connection ~ 6850 2800
Connection ~ 6850 2900
Wire Wire Line
	7350 2400 7350 3000
Connection ~ 7350 2500
Connection ~ 7350 2600
Connection ~ 7350 2700
Connection ~ 7350 2800
Connection ~ 7350 2900
Wire Wire Line
	6300 2400 7350 2400
Text Label 6300 1700 0    60   ~ 0
Drive_Vcc
Text Label 6300 2400 0    60   ~ 0
Drive_GND
Connection ~ 6850 1700
Connection ~ 6850 2400
$Comp
L CONN_02X19 P7
U 1 1 56D8D854
P 9400 2100
F 0 "P7" H 9400 3100 50  0000 C CNN
F 1 "CONN_02X19" V 9400 2100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19" H 9400 1100 50  0001 C CNN
F 3 "" H 9400 1100 50  0000 C CNN
	1    9400 2100
	1    0    0    -1  
$EndComp
Text Label 8600 1200 0    60   ~ 0
High_1_0
Text Label 8600 1300 0    60   ~ 0
High_1_1
Text Label 8600 1400 0    60   ~ 0
Low_1_0
Text Label 8600 1500 0    60   ~ 0
Low_1_1
Text Label 8600 1600 0    60   ~ 0
GND
Wire Wire Line
	8600 1200 9650 1200
Wire Wire Line
	8600 1300 9650 1300
Wire Wire Line
	8600 1400 9650 1400
Wire Wire Line
	8600 1500 9650 1500
Wire Wire Line
	8600 1600 9650 1600
Connection ~ 9150 1200
Connection ~ 9150 1300
Connection ~ 9150 1400
Connection ~ 9150 1500
Connection ~ 9150 1600
Wire Wire Line
	9150 1700 9150 2300
Connection ~ 9150 1800
Connection ~ 9150 1900
Connection ~ 9150 2000
Connection ~ 9150 2100
Wire Wire Line
	9650 1700 9650 2300
Connection ~ 9650 2100
Connection ~ 9650 2000
Connection ~ 9650 1900
Connection ~ 9650 1800
Wire Wire Line
	8600 1700 9650 1700
Connection ~ 9150 2200
Connection ~ 9650 2200
Wire Wire Line
	9150 2400 9150 3000
Connection ~ 9150 2500
Connection ~ 9150 2600
Connection ~ 9150 2700
Connection ~ 9150 2800
Connection ~ 9150 2900
Wire Wire Line
	9650 2400 9650 3000
Connection ~ 9650 2500
Connection ~ 9650 2600
Connection ~ 9650 2700
Connection ~ 9650 2800
Connection ~ 9650 2900
Wire Wire Line
	8600 2400 9650 2400
Text Label 8600 1700 0    60   ~ 0
Drive_Vcc
Text Label 8600 2400 0    60   ~ 0
Drive_GND
Connection ~ 9150 1700
Connection ~ 9150 2400
$Comp
L CONN_02X19 P6
U 1 1 56D8D91B
P 7100 4300
F 0 "P6" H 7100 5300 50  0000 C CNN
F 1 "CONN_02X19" V 7100 4300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19" H 7100 3300 50  0001 C CNN
F 3 "" H 7100 3300 50  0000 C CNN
	1    7100 4300
	1    0    0    -1  
$EndComp
Text Label 6300 3400 0    60   ~ 0
High_2_0
Text Label 6300 3500 0    60   ~ 0
High_2_1
Text Label 6300 3600 0    60   ~ 0
Low_2_0
Text Label 6300 3700 0    60   ~ 0
Low_2_1
Text Label 6300 3800 0    60   ~ 0
GND
Wire Wire Line
	6300 3400 7350 3400
Wire Wire Line
	6300 3500 7350 3500
Wire Wire Line
	6300 3600 7350 3600
Wire Wire Line
	6300 3700 7350 3700
Wire Wire Line
	6300 3800 7350 3800
Connection ~ 6850 3400
Connection ~ 6850 3500
Connection ~ 6850 3600
Connection ~ 6850 3700
Connection ~ 6850 3800
Wire Wire Line
	6850 3900 6850 4500
Connection ~ 6850 4000
Connection ~ 6850 4100
Connection ~ 6850 4200
Connection ~ 6850 4300
Wire Wire Line
	7350 3900 7350 4500
Connection ~ 7350 4300
Connection ~ 7350 4200
Connection ~ 7350 4100
Connection ~ 7350 4000
Wire Wire Line
	6300 3900 7350 3900
Connection ~ 6850 4400
Connection ~ 7350 4400
Wire Wire Line
	6850 4600 6850 5200
Connection ~ 6850 4700
Connection ~ 6850 4800
Connection ~ 6850 4900
Connection ~ 6850 5000
Connection ~ 6850 5100
Wire Wire Line
	7350 4600 7350 5200
Connection ~ 7350 4700
Connection ~ 7350 4800
Connection ~ 7350 4900
Connection ~ 7350 5000
Connection ~ 7350 5100
Wire Wire Line
	6300 4600 7350 4600
Text Label 6300 3900 0    60   ~ 0
Drive_Vcc
Text Label 6300 4600 0    60   ~ 0
Drive_GND
Connection ~ 6850 3900
Connection ~ 6850 4600
$Comp
L CONN_02X19 P8
U 1 1 56D8D94E
P 9400 4300
F 0 "P8" H 9400 5300 50  0000 C CNN
F 1 "CONN_02X19" V 9400 4300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x19" H 9400 3300 50  0001 C CNN
F 3 "" H 9400 3300 50  0000 C CNN
	1    9400 4300
	1    0    0    -1  
$EndComp
Text Label 8600 3400 0    60   ~ 0
High_3_0
Text Label 8600 3500 0    60   ~ 0
High_3_1
Text Label 8600 3600 0    60   ~ 0
Low_3_0
Text Label 8600 3700 0    60   ~ 0
Low_3_1
Text Label 8600 3800 0    60   ~ 0
GND
Wire Wire Line
	8600 3400 9650 3400
Wire Wire Line
	8600 3500 9650 3500
Wire Wire Line
	8600 3600 9650 3600
Wire Wire Line
	8600 3700 9650 3700
Wire Wire Line
	8600 3800 9650 3800
Connection ~ 9150 3400
Connection ~ 9150 3500
Connection ~ 9150 3600
Connection ~ 9150 3700
Connection ~ 9150 3800
Wire Wire Line
	9150 3900 9150 4500
Connection ~ 9150 4000
Connection ~ 9150 4100
Connection ~ 9150 4200
Connection ~ 9150 4300
Wire Wire Line
	9650 3900 9650 4500
Connection ~ 9650 4300
Connection ~ 9650 4200
Connection ~ 9650 4100
Connection ~ 9650 4000
Wire Wire Line
	8600 3900 9650 3900
Connection ~ 9150 4400
Connection ~ 9650 4400
Wire Wire Line
	9150 4600 9150 5200
Connection ~ 9150 4700
Connection ~ 9150 4800
Connection ~ 9150 4900
Connection ~ 9150 5000
Connection ~ 9150 5100
Wire Wire Line
	9650 4600 9650 5200
Connection ~ 9650 4700
Connection ~ 9650 4800
Connection ~ 9650 4900
Connection ~ 9650 5000
Connection ~ 9650 5100
Wire Wire Line
	8600 4600 9650 4600
Text Label 8600 3900 0    60   ~ 0
Drive_Vcc
Text Label 8600 4600 0    60   ~ 0
Drive_GND
Connection ~ 9150 3900
Connection ~ 9150 4600
$Comp
L ATMEGA328-P IC1
U 1 1 56D8D987
P 4100 6250
F 0 "IC1" H 3350 7500 50  0000 L BNN
F 1 "ATMEGA328-P" H 4500 4850 50  0000 L BNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 4100 6250 50  0001 C CIN
F 3 "" H 4100 6250 50  0000 C CNN
	1    4100 6250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P2
U 1 1 56D9A03B
P 4200 1250
F 0 "P2" H 4200 1350 50  0000 C CNN
F 1 "CONN_01X01" V 4300 1250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 4200 1250 50  0001 C CNN
F 3 "" H 4200 1250 50  0000 C CNN
	1    4200 1250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P3
U 1 1 56D9A08E
P 4200 1550
F 0 "P3" H 4200 1650 50  0000 C CNN
F 1 "CONN_01X01" V 4300 1550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 4200 1550 50  0001 C CNN
F 3 "" H 4200 1550 50  0000 C CNN
	1    4200 1550
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X06 P1
U 1 1 56D9A0BA
P 2750 1400
F 0 "P1" H 2750 1750 50  0000 C CNN
F 1 "CONN_01X06" V 2850 1400 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06" H 2750 1400 50  0001 C CNN
F 3 "" H 2750 1400 50  0000 C CNN
	1    2750 1400
	1    0    0    -1  
$EndComp
Text Label 1200 1150 0    60   ~ 0
Direct_Vcc
Text Label 1200 1250 0    60   ~ 0
Direct_GND
Text Label 1200 1350 0    60   ~ 0
SCK
Text Label 5300 5150 0    60   ~ 0
High_0_0
Text Label 5300 5250 0    60   ~ 0
High_0_1
Text Label 5300 5750 0    60   ~ 0
Low_0_0
Text Label 5300 5850 0    60   ~ 0
Low_0_1
Text Label 5300 6000 0    60   ~ 0
High_1_0
Text Label 5300 6100 0    60   ~ 0
High_1_1
Text Label 5300 6200 0    60   ~ 0
Low_1_0
Text Label 5300 6300 0    60   ~ 0
Low_1_1
Text Label 5300 6400 0    60   ~ 0
High_2_0
Text Label 5300 6500 0    60   ~ 0
High_2_1
Text Label 5300 6750 0    60   ~ 0
Low_2_0
Text Label 5300 6850 0    60   ~ 0
Low_2_1
Text Label 5300 6950 0    60   ~ 0
High_3_0
Text Label 5300 7050 0    60   ~ 0
High_3_1
Text Label 5300 7150 0    60   ~ 0
Low_3_0
Text Label 5300 7250 0    60   ~ 0
Low_3_1
Wire Wire Line
	5100 5150 5300 5150
Wire Wire Line
	5300 5250 5100 5250
Wire Wire Line
	5100 5750 5300 5750
Wire Wire Line
	5300 5850 5100 5850
Wire Wire Line
	5100 6000 5300 6000
Wire Wire Line
	5300 6100 5100 6100
Wire Wire Line
	5100 6200 5300 6200
Wire Wire Line
	5300 6300 5100 6300
Wire Wire Line
	5100 6400 5300 6400
Wire Wire Line
	5300 6500 5100 6500
Wire Wire Line
	5100 6750 5300 6750
Wire Wire Line
	5300 6850 5100 6850
Wire Wire Line
	5100 6950 5300 6950
Wire Wire Line
	5300 7050 5100 7050
Wire Wire Line
	5100 7150 5300 7150
Wire Wire Line
	5300 7250 5100 7250
$Comp
L R R5
U 1 1 56DC2E8F
P 5400 7350
F 0 "R5" V 5480 7350 50  0000 C CNN
F 1 "R" V 5400 7350 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 5330 7350 50  0001 C CNN
F 3 "" H 5400 7350 50  0000 C CNN
	1    5400 7350
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 56DC2EBD
P 5650 7450
F 0 "R6" V 5730 7450 50  0000 C CNN
F 1 "R" V 5650 7450 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 5580 7450 50  0001 C CNN
F 3 "" H 5650 7450 50  0000 C CNN
	1    5650 7450
	0    1    1    0   
$EndComp
$Comp
L LED D2
U 1 1 56DC2F40
P 5900 7350
F 0 "D2" H 5900 7450 50  0000 C CNN
F 1 "LED" H 5900 7250 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 5900 7350 50  0001 C CNN
F 3 "" H 5900 7350 50  0000 C CNN
	1    5900 7350
	-1   0    0    1   
$EndComp
$Comp
L LED D3
U 1 1 56DC2F77
P 6250 7450
F 0 "D3" H 6250 7550 50  0000 C CNN
F 1 "LED" H 6250 7350 50  0000 C CNN
F 2 "LEDs:LED-3MM" H 6250 7450 50  0001 C CNN
F 3 "" H 6250 7450 50  0000 C CNN
	1    6250 7450
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 7350 5100 7350
Wire Wire Line
	5500 7450 5100 7450
Wire Wire Line
	5550 7350 5700 7350
Wire Wire Line
	5800 7450 6050 7450
Text Label 1200 1450 0    60   ~ 0
SS
Text Label 1200 1550 0    60   ~ 0
MOSI
Text Label 1200 1650 0    60   ~ 0
MISO
Text Label 1200 3650 0    60   ~ 0
Direct_Vcc
Text Label 1200 4650 0    60   ~ 0
Direct_GND
$Comp
L C C1
U 1 1 56DC3BF9
P 2550 4100
F 0 "C1" H 2575 4200 50  0000 L CNN
F 1 "C" H 2575 4000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2588 3950 50  0001 C CNN
F 3 "" H 2550 4100 50  0000 C CNN
	1    2550 4100
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 56DC3C54
P 2100 3900
F 0 "R2" V 2180 3900 50  0000 C CNN
F 1 "R" V 2100 3900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 2030 3900 50  0001 C CNN
F 3 "" H 2100 3900 50  0000 C CNN
	1    2100 3900
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 56DC3C97
P 2100 4350
F 0 "D1" H 2100 4450 50  0000 C CNN
F 1 "LED" H 2100 4250 50  0000 C CNN
F 2 "LEDs:LED-5MM" H 2100 4350 50  0001 C CNN
F 3 "" H 2100 4350 50  0000 C CNN
	1    2100 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1200 3650 2950 3650
Wire Wire Line
	2100 3650 2100 3750
Wire Wire Line
	2100 4050 2100 4150
Wire Wire Line
	2100 4550 2100 4650
Wire Wire Line
	1200 4650 2950 4650
Text Label 2950 3650 0    60   ~ 0
Vcc
Text Label 2950 4650 0    60   ~ 0
GND
Connection ~ 2100 3650
Connection ~ 2100 4650
Wire Wire Line
	2550 3950 2550 3650
Connection ~ 2550 3650
Wire Wire Line
	2550 4250 2550 4650
Connection ~ 2550 4650
Wire Wire Line
	1100 1150 2550 1150
Wire Wire Line
	700  1250 2550 1250
Wire Wire Line
	1200 1350 2550 1350
Wire Wire Line
	1200 1450 2550 1450
Wire Wire Line
	1850 1550 1200 1550
Wire Wire Line
	1200 1650 2150 1650
Text Label 3350 1250 0    60   ~ 0
Drive_Vcc
Text Label 3350 1550 0    60   ~ 0
Drive_GND
Wire Wire Line
	3350 1250 4000 1250
Wire Wire Line
	4000 1550 3350 1550
Text Label 2300 5150 0    60   ~ 0
Vcc
Text Label 2350 7350 0    60   ~ 0
GND
Wire Wire Line
	2300 5150 3200 5150
Wire Wire Line
	2350 7350 3200 7350
Wire Wire Line
	3200 7450 3000 7450
Wire Wire Line
	3000 7450 3000 7350
Connection ~ 3000 7350
Text Label 5300 5350 0    60   ~ 0
SS
Text Label 5300 5450 0    60   ~ 0
MOSI
Text Label 5300 5550 0    60   ~ 0
MISO
Text Label 5300 5650 0    60   ~ 0
SCK
Wire Wire Line
	5100 5350 5300 5350
Wire Wire Line
	5300 5450 5100 5450
Wire Wire Line
	5100 5550 5300 5550
Wire Wire Line
	5100 5650 5300 5650
$Comp
L CONN_02X03 P4
U 1 1 56DC5831
P 5250 1400
F 0 "P4" H 5250 1600 50  0000 C CNN
F 1 "CONN_02X03" H 5250 1200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 5250 200 50  0001 C CNN
F 3 "" H 5250 200 50  0000 C CNN
	1    5250 1400
	1    0    0    -1  
$EndComp
Text Label 4600 1300 0    60   ~ 0
GND
Text Label 4600 1400 0    60   ~ 0
MOSI
Text Label 4600 1500 0    60   ~ 0
VCC
Text Label 5750 1300 0    60   ~ 0
Reset
Text Label 5750 1400 0    60   ~ 0
SCK
Text Label 5750 1500 0    60   ~ 0
MISO
Wire Wire Line
	4600 1300 5000 1300
Wire Wire Line
	4600 1400 5000 1400
Wire Wire Line
	5000 1500 4600 1500
Wire Wire Line
	5750 1300 5500 1300
Wire Wire Line
	5500 1400 5750 1400
Wire Wire Line
	5750 1500 5500 1500
Text Label 5300 6600 0    60   ~ 0
Reset
Wire Wire Line
	5300 6600 5100 6600
$Comp
L R R4
U 1 1 56DF85A8
P 4250 2250
F 0 "R4" V 4330 2250 50  0000 C CNN
F 1 "R" V 4250 2250 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 4180 2250 50  0001 C CNN
F 3 "" H 4250 2250 50  0000 C CNN
	1    4250 2250
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 56DF8615
P 4250 2850
F 0 "SW1" H 4400 2960 50  0000 C CNN
F 1 "SW_PUSH" H 4250 2770 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" H 4250 2850 50  0001 C CNN
F 3 "" H 4250 2850 50  0000 C CNN
	1    4250 2850
	0    1    1    0   
$EndComp
Text Label 4250 2000 0    60   ~ 0
Vcc
Text Label 4550 2500 0    60   ~ 0
Reset
Text Label 4250 3300 0    60   ~ 0
GND
Wire Wire Line
	4250 2000 4250 2100
Wire Wire Line
	4250 2400 4250 2550
Wire Wire Line
	4250 3150 4250 3300
Wire Wire Line
	4550 2500 4250 2500
Connection ~ 4250 2500
$Comp
L R R1
U 1 1 56DF8BD6
P 2000 1550
F 0 "R1" V 2080 1550 50  0000 C CNN
F 1 "R" V 2000 1550 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 1930 1550 50  0001 C CNN
F 3 "" H 2000 1550 50  0000 C CNN
	1    2000 1550
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 56DF8C4E
P 2300 1650
F 0 "R3" V 2380 1650 50  0000 C CNN
F 1 "R" V 2300 1650 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 2230 1650 50  0001 C CNN
F 3 "" H 2300 1650 50  0000 C CNN
	1    2300 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 1550 2550 1550
Wire Wire Line
	2550 1650 2450 1650
NoConn ~ 3200 5450
NoConn ~ 3200 5750
$Comp
L PWR_FLAG #FLG01
U 1 1 56DF9807
P 1100 1050
F 0 "#FLG01" H 1100 1145 50  0001 C CNN
F 1 "PWR_FLAG" H 1100 1230 50  0000 C CNN
F 2 "" H 1100 1050 50  0000 C CNN
F 3 "" H 1100 1050 50  0000 C CNN
	1    1100 1050
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 56DF9855
P 700 1050
F 0 "#FLG02" H 700 1145 50  0001 C CNN
F 1 "PWR_FLAG" H 700 1230 50  0000 C CNN
F 2 "" H 700 1050 50  0000 C CNN
F 3 "" H 700 1050 50  0000 C CNN
	1    700  1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1050 1100 1150
Wire Wire Line
	700  1250 700  1050
Text Label 6600 7600 0    60   ~ 0
GND
Wire Wire Line
	6100 7350 6600 7350
Wire Wire Line
	6600 7350 6600 7600
Wire Wire Line
	6450 7450 6600 7450
Connection ~ 6600 7450
$Comp
L CONN_01X06 P9
U 1 1 56DF9583
P 2750 2550
F 0 "P9" H 2750 2900 50  0000 C CNN
F 1 "CONN_01X06" V 2850 2550 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06" H 2750 2550 50  0001 C CNN
F 3 "" H 2750 2550 50  0000 C CNN
	1    2750 2550
	1    0    0    -1  
$EndComp
Text Label 1250 2300 0    60   ~ 0
Direct_GND
Text Label 1250 2500 0    60   ~ 0
Direct_Vcc
Text Label 1250 2600 0    60   ~ 0
Low_2_0
Text Label 1250 2700 0    60   ~ 0
Low_2_1
Wire Wire Line
	1250 2300 2550 2300
Wire Wire Line
	2550 2500 1250 2500
Wire Wire Line
	1250 2600 2550 2600
Wire Wire Line
	2550 2700 1250 2700
NoConn ~ 2550 2400
NoConn ~ 2550 2800
$EndSCHEMATC
